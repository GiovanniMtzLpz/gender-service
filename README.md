# gender-service

## Para poder Realizar las peticiones use lo siguiente 🚀
 
* Pueden entrar a la url: http://13.58.19.102:8080/swagger-ui.html y usar el el swagger para realizar peticiones

## Para poder Realizar las peticiones desde un postman use lo siguiente.

* URL:  http://13.58.19.102:8080/googleMaps/getMaps

## Metodo

* GET

## Headers.
* latitud: 151.1957362
* longitud: -33.8670522
* radius: 1500
* type: restaurant
---

