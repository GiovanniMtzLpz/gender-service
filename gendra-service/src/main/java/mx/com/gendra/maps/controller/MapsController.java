package mx.com.gendra.maps.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import mx.com.gendra.maps.service.IMapsService; 
import mx.com.gendra.maps.util.MapsConstantes;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping(MapsConstantes.GOOGLEMAPS)

public class MapsController {
 
	@Autowired
	private IMapsService mapsService;
	/*
	 * Este metodo al servicio a consumir
	 * 
	 */
	
	@GetMapping(MapsConstantes.GETMAPS)
	public ResponseEntity<?> getMaps(@RequestHeader String longitud,
			@RequestHeader String latitud,
			@RequestHeader String radius,
			@RequestHeader String type) {
		
		return new ResponseEntity<Object>(mapsService.getDataMaps(longitud,latitud,radius,type), HttpStatus.OK);
	}
	
}