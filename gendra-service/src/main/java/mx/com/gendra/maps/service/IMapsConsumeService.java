package mx.com.gendra.maps.service;
import java.util.Map;
import org.springframework.http.HttpEntity; 
import org.springframework.http.HttpMethod; 
public interface IMapsConsumeService {
	
	Map<String, Object> consumeMaps(String url,HttpMethod httpMethod, String logDescripcionOk,
            String logDescripcionError, HttpEntity<Object> entityRetry);
	
 
}
