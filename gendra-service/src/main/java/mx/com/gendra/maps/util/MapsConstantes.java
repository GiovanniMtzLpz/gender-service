package mx.com.gendra.maps.util;
/*
 * Este clase se declaran las constantes a utilizar
 */
public final class MapsConstantes {
    
	public static final String GOOGLEMAPS = "/googleMaps"; 
	public static final String GETMAPS = "/getMaps"; 
	public static final String ERROR = "error"; 
	public static final String OK = "ok"; 
	public static final String LOCATION = "?location="; 
	public static final String RADIUS = "&radius="; 
	public static final String TYPE = "&type="; 
	public static final String KEY = "&key=";
	
	
	private MapsConstantes() {
		throw new IllegalStateException("MapsConstantes class");
	}  

}