package mx.com.gendra.maps.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Utils {
	/*
	 * Este metodo realiza el armado de la longitud y la latitud
	 */
	public static String getLocation(String longitud, String latitud ) {
		return longitud+","+latitud;
	}
	
	/**
	 * 
	 * @param url  url a usar
	 * @param location  se usa para la localizacion
	 * @param radius  el dario que se quiere realizar la busqueda
	 * @param type  que quieres buscar
	 * @param key  el id a usar para el consumo
	 * @return  responde la url armada
	 */
	public static StringBuilder getUrl(String url, String location,String radius,String type,String key) {
		StringBuilder urlData = new StringBuilder();
		return urlData.append(url).append(MapsConstantes.LOCATION).append(location).append(MapsConstantes.RADIUS).
		append(radius).append(MapsConstantes.TYPE).append(type).append(MapsConstantes.KEY).append(key);
		 
	}

	/**
	 * 
	 * @param mapa que contiene toda la respuesta del servicio consumido
	 * @return reaponde los datos que vamos a enviar en la respuesta
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ArrayList<Map<String, Object>> getData(Map<String, Object> map) {  
		ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) map.get("results"); 
		
		ArrayList<Map<String, Object>> listResponse = new ArrayList<Map<String,Object>>(); 
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			
			Map<String, Object> mapData = (Map<String, Object>) iterator.next();
			Map<String, Object> geometry = (Map<String, Object>) mapData.get("geometry");
			Map<String, Object> location = (Map<String, Object>) geometry.get("location");

			Map<String,Object> response = new HashMap<String, Object>();
			response.put("latitude",location.get("lat")); 
			response.put("longitude",location.get("lng"));
			response.put("name",mapData.get("vicinity")); 
			response.put("score",mapData.get("price_level"));
 
			System.out.println("datosssss   "+response); 
			listResponse.add(response);
		} 
		return listResponse;
	}
}
