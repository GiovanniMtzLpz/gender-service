package mx.com.gendra.maps.service;

import java.util.ArrayList;
import java.util.Map;
 
public interface IMapsService {  
	ArrayList<Map<String , Object>> getDataMaps(String longitud,String latitud,String radius,String type);
}
