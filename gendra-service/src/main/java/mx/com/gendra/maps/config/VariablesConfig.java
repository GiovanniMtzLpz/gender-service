package mx.com.gendra.maps.config;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
/*
 * Este clase es para traer las variables  del archivo de config
 */
@Configuration
@ConfigurationProperties(prefix="variables")
@Validated
public class VariablesConfig {
	@NotEmpty
	private String url;

	@NotEmpty
	private String key;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	

}
