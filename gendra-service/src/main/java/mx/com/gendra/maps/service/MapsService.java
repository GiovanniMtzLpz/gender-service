
package mx.com.gendra.maps.service;
 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import mx.com.gendra.maps.config.VariablesConfig;
import mx.com.gendra.maps.util.MapsConstantes;
import mx.com.gendra.maps.util.Utils;

 
@Service
public class MapsService implements IMapsService {

	@Autowired 
	private IMapsConsumeService serviceBaseService;
	
	@Autowired 
	private VariablesConfig variablesConfig;
	/*
	 * Este metodo Realiza el consumo del servicio 
	 *
	 * 
	 */
	@Override
	public  ArrayList<Map<String, Object>>  getDataMaps(String longitud,String latitud,String radius,String type) {
		HttpEntity<Object> entityRetry = new HttpEntity<Object>(null);
		
		Map<String, Object> consumirMapa = new HashMap<String, Object>();
		StringBuilder url = Utils.getUrl(variablesConfig.getUrl(),
				Utils.getLocation(longitud, latitud),radius,type,variablesConfig.getKey());
		System.out.println("URL  "+url);
		consumirMapa=serviceBaseService.consumeMaps(url.toString(), 
				HttpMethod.GET, MapsConstantes.OK,MapsConstantes.ERROR,entityRetry); 
		return  Utils.getData(consumirMapa);
		
	}
	
	
}
