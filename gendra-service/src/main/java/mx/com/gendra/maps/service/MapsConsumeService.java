 
package mx.com.gendra.maps.service;
 
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import mx.com.gendra.maps.util.MapsConstantes;  
 
@Service
public class MapsConsumeService implements IMapsConsumeService {
	
	@Autowired
	RestTemplate restTemplate;
 
    /** La Constante LOGGER. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(MapsConsumeService.class);
	/*
	 * Este metodo Realiza la reunion de los datos para consumir
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> consumeMaps(String url, HttpMethod httpMethod, String logDescripcionOk,
			String logDescripcionError, HttpEntity<Object> entityRetry) {
		ResponseEntity<Object> response = exchange(url,httpMethod, entityRetry);
 
		return (Map<String, Object>) response.getBody();
	}
	  
	private ResponseEntity<Object> exchange(String url, HttpMethod metodo,HttpEntity<Object> entityRetry ) {
		ResponseEntity<Object> entitys = new ResponseEntity<>( HttpStatus.BAD_REQUEST );
        try {
        	 entitys = restTemplate.exchange(url, metodo, entityRetry, Object.class);  
        } catch (RestClientException e) {
        	LOGGER.error(MapsConstantes.ERROR,e);
        }
		return entitys;
	}


 
	 
}
